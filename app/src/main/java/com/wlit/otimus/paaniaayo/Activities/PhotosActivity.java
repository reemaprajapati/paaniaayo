package com.wlit.otimus.paaniaayo.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wlit.otimus.paaniaayo.R;
import com.wlit.otimus.paaniaayo.Adapter.PhotosPagerAdapater;
import com.wlit.otimus.paaniaayo.Model.Photos;
import com.wlit.otimus.paaniaayo.Model.TouchImageView;
import com.wlit.otimus.paaniaayo.Rest.ApiClient;
import com.wlit.otimus.paaniaayo.Rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotosActivity extends AppCompatActivity {
    Integer position;

    List<Photos> photosList=new ArrayList<>();
    List<TouchImageView> images=new ArrayList<>();
    ImageView close;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        Intent intent=getIntent();
       position = (Integer) intent.getSerializableExtra("place_id");
        close= (ImageView) findViewById(R.id.close_icon);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        //Api call using Retrofit
        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<List<Photos>> call=apiInterface.getPhotos(position);
        call.enqueue(new Callback<List<Photos>>() {
            @Override
            public void onResponse(Call<List<Photos>> call, Response<List<Photos>> response) {
                photosList.addAll(response.body());

                Log.d("sizeofresponse",String.valueOf(photosList.size()));
                for(int i=0;i<photosList.size();i++){
                    final TouchImageView imageView=new TouchImageView(getApplicationContext());
                    //using glide library to load images from server

                    Glide.with(imageView.getContext())
                            .load(photosList.get(i).getImage_url())
                             .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            imageView.setImageBitmap(resource);
                        }
                    });
                    Log.d("imageUrl",photosList.get(i).getImage_url());
                    images.add(imageView);

                }

                //setting up pageradapter
                PhotosPagerAdapater pageradapter = new PhotosPagerAdapater(images);
                ViewPager viewpager = (ViewPager) findViewById(R.id.pager);

                viewpager.setAdapter(pageradapter);
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                // Show images following the position
                viewpager.setCurrentItem(0);
                viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });


            }

            @Override
            public void onFailure(Call<List<Photos>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Snackbar mySnackbar = Snackbar.make(findViewById(android.R.id.content),"No Internet Connection",Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setAction("TRY AGAIN",new myUndoListener());
                mySnackbar.show();
            }
        });



    }
    public  class myUndoListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            finish();
            startActivity(getIntent());
        }
    }


}

