package com.wlit.otimus.paaniaayo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.wlit.otimus.paaniaayo.R;
import com.wlit.otimus.paaniaayo.Model.PlacesItem;

import java.util.List;

/**
 * Created by Otimus on 2/2/2017.
 */

public class PlacesItemAdapter extends RecyclerView.Adapter<PlacesItemAdapter.MyViewHolder> {
    private int lastPosition = -1;

    List<PlacesItem> placesItems;
    private final OnItemClickListener listener;
    Context context;

    public PlacesItemAdapter(Context context,List<PlacesItem> placesItems,  OnItemClickListener listener) {
        this.placesItems = placesItems;
        this.listener = listener;
        this.context=context;
    }
    public interface OnItemClickListener{
        void onItemClick(PlacesItem item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_places,parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(placesItems.get(position),listener);

        holder.id.setText(String.valueOf(placesItems.get(position).getId()));
        holder.name.setText(placesItems.get(position).getName());
        holder.published_date.setText(placesItems.get(position).getPublished_date());
        Log.d("output1",placesItems.get(position).getName());
        setAnimation(holder.itemView, position);

    }


    @Override
    public int getItemCount() {
        return placesItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id,name,published_date;

        public MyViewHolder(View itemView) {
            super(itemView);
            id= (TextView) itemView.findViewById(R.id.place_id);
            name=(TextView)itemView.findViewById(R.id.place_name);
            published_date=(TextView) itemView.findViewById(R.id.itempublisheddate);

        }
        public void bind(final PlacesItem item, final OnItemClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(item);

                }
            });
        }
         public void clearAnimation()
        {
            itemView.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        ((MyViewHolder)holder).clearAnimation();
    }



}

