package com.wlit.otimus.paaniaayo.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wlit.otimus.paaniaayo.R;

public class DetailActivity extends AppCompatActivity {

    TextView place_name, municipalities,ward, published_date;
    Button view_schedule;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        place_name= (TextView) findViewById(R.id.d_place);
        municipalities= (TextView) findViewById(R.id.d_municipalities);
        ward= (TextView) findViewById(R.id.d_ward);
        published_date= (TextView) findViewById(R.id.d_published_date);
        view_schedule= (Button) findViewById(R.id.button_view);

        Intent intent=getIntent();
       final Integer id= (Integer) intent.getSerializableExtra("id");
        String s_name= (String) intent.getSerializableExtra("name");
        String s_municipalities= (String) intent.getSerializableExtra("municipalities");
        String s_ward= (String) intent.getSerializableExtra("ward");
        String s_published_date= (String) intent.getSerializableExtra("published_date");

        place_name.setText(s_name);
        municipalities.setText(s_municipalities);
        ward.setText(s_ward);
        published_date.setText(s_published_date);

        view_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),PhotosActivity.class);
                intent.putExtra("place_id",id);
                startActivity(intent);
            }
        });


    }
}
