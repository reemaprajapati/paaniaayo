package com.wlit.otimus.paaniaayo.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.wlit.otimus.paaniaayo.AlarmReceiver;
import com.wlit.otimus.paaniaayo.R;

import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {

    TimePicker timePicker;
    ToggleButton toggleButton;
    TextView textView;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;
   private static AlarmActivity inst;
    public static AlarmActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AlarmReceiver.stopRingtone();
        timePicker= (TimePicker) findViewById(R.id.timePicker);
        toggleButton= (ToggleButton) findViewById(R.id.toggleButton);
        textView= (TextView) findViewById(R.id.alarmText);
        alarmManager= (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);


    }
    public void onToggleClicked(View view){
        if(((ToggleButton)view).isChecked()){
            Log.d("Alarm","Alarm On");
          Calendar calendar=Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY ,timePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
            Intent intent= new Intent(this, AlarmReceiver.class);
            pendingIntent=PendingIntent.getBroadcast(this,0,intent,0);
            alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
        }
        else {
            AlarmReceiver.stopRingtone();

            Intent myIntent = new Intent(this,
                    AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0,
                    myIntent, 0);
            alarmManager.cancel(pendingIntent);
            setAlarmText("");
            Log.d("Alarm","Alarm Off");
        }
    }

    public  void setAlarmText(String alarmText){
        textView.setText(alarmText);
    }
}
