package com.wlit.otimus.paaniaayo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ichha on 2/11/2017.
 */

public class Photos {

    @SerializedName("id")
    Integer id;

    @SerializedName("place_id")
    Integer place_id;

    @SerializedName("image_url")
    String image_url;

    @SerializedName("published_date")
    String published_date;

    public Photos(Integer id, Integer place_id, String image_url, String published_date) {
        this.id = id;
        this.place_id = place_id;
        this.image_url = image_url;
        this.published_date = published_date;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlace_id() {
        return place_id;
    }

    public void setPlace_id(Integer place_id) {
        this.place_id = place_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getPublished_date() {
        return published_date;
    }

    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }
}
