package com.wlit.otimus.paaniaayo.Adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;


import com.wlit.otimus.paaniaayo.Model.TouchImageView;

import java.util.List;

/**
 * Created by Otimus on 2/11/2017.
 */

public class PhotosPagerAdapater extends PagerAdapter {

    List<TouchImageView> images;

    public PhotosPagerAdapater(List<TouchImageView> images) {
        this.images = images;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imageView=images.get(position);
        container.addView(imageView);
        return imageView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(images.get(position));

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
