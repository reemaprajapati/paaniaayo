package com.wlit.otimus.paaniaayo.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wlit.otimus.paaniaayo.R;
import com.wlit.otimus.paaniaayo.Activities.DetailActivity;
import com.wlit.otimus.paaniaayo.Adapter.PlacesItemAdapter;
import com.wlit.otimus.paaniaayo.Model.PlacesItem;
import com.wlit.otimus.paaniaayo.Rest.ApiClient;
import com.wlit.otimus.paaniaayo.Rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentList extends Fragment {

    RecyclerView recyclerView;
    ArrayList<PlacesItem> placesItems=new ArrayList<>();

    PlacesItemAdapter placesItemAdapter;
    ProgressDialog mProgressDialog;

    public FragmentList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        placesItemAdapter= new PlacesItemAdapter(getActivity(), placesItems, new PlacesItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PlacesItem item) {
                Intent intent= new Intent(getContext(), DetailActivity.class);
                intent.putExtra("id",item.getId());
                intent.putExtra("name",item.getName());
                intent.putExtra("municipalities",item.getMunicipalities());
                intent.putExtra("ward",item.getWard());
                intent.putExtra("published_date",item.getPublished_date());

               startActivity(intent);

//                Log.d("name", item.getOff_name());
            }
        });
        recyclerView.setAdapter(placesItemAdapter);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<List<PlacesItem>> call=apiInterface.getPlaces();
        call.enqueue(new Callback<List<PlacesItem>>() {
            @Override
            public void onResponse(Call<List<PlacesItem>> call, Response<List<PlacesItem>> response) {

                placesItems.addAll(response.body());
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

                placesItemAdapter.notifyDataSetChanged();
//                recyclerView.setAdapter(placesItemAdapter);
//
                Log.d("item",String.valueOf(placesItems.size()));
                Log.d("output",String.valueOf(placesItems.get(1).getName()));
            }

            @Override
            public void onFailure(Call<List<PlacesItem>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
               Snackbar mySnackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),"No Internet Connection",Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setAction("TRY AGAIN",new myUndoListener());
                mySnackbar.show();
//                Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;

  }

    public  class myUndoListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            getActivity().finish();
            startActivity(getActivity().getIntent());
        }
    }
}
