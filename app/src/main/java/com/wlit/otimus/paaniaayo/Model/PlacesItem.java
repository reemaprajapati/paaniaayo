package com.wlit.otimus.paaniaayo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Otimus on 2/1/2017.
 */

public class PlacesItem {

    @SerializedName("id")
    Integer id;

    @SerializedName("name")
    String name;

    @SerializedName("municipalities")
    String municipalities;

    @SerializedName("ward")
    String ward;

    @SerializedName("published_date")
    String published_date;

    @SerializedName("gps_lat")
    Double gps_lat;

    @SerializedName("gps_long")
    Double gps_long;

    public PlacesItem(Integer id, String name, String municipalities, String ward, String published_date, Double gps_lat, Double gps_long) {
        this.id = id;
        this.name = name;
        this.municipalities = municipalities;
        this.ward = ward;
        this.published_date = published_date;
        this.gps_lat = gps_lat;
        this.gps_long = gps_long;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(String municipalities) {
        this.municipalities = municipalities;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getPublished_date() {
        return published_date;
    }

    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }

    public Double getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(Double gps_lat) {
        this.gps_lat = gps_lat;
    }

    public Double getGps_long() {
        return gps_long;
    }

    public void setGps_long(Double gps_long) {
        this.gps_long = gps_long;
    }
}