package com.wlit.otimus.paaniaayo.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wlit.otimus.paaniaayo.R;
import com.wlit.otimus.paaniaayo.Activities.DetailActivity;
import com.wlit.otimus.paaniaayo.Model.PlacesItem;
import com.wlit.otimus.paaniaayo.Rest.ApiClient;
import com.wlit.otimus.paaniaayo.Rest.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMap extends Fragment implements GoogleMap.OnInfoWindowClickListener {

    MapView mMapView;
    private GoogleMap googleMap;
    ArrayList<PlacesItem> placesItems = new ArrayList<>();
    ArrayList<LatLng> latlngs = new ArrayList<>();
    private Marker myMarker;


    public FragmentMap() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); //get map ready

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1);
                    return;
                }
                googleMap.setMyLocationEnabled(true);


                    generateData();// getting data from database


                }
            });

            return rootView;
        }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        generateData();
    }

    public void generateData(){

            ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
            Call<List<PlacesItem>> call=apiInterface.getPlaces();
            call.enqueue(new Callback<List<PlacesItem>>() {
                @Override
                public void onResponse(Call<List<PlacesItem>> call, Response<List<PlacesItem>> response) {
                   placesItems.addAll(response.body());

//                    Log.d("latitude",placesItems.get(1).getGps_lat().toString());
                    for(int i = 0 ; i < placesItems.size() ; i++ ) {
                    //creating markers
                        createMarker(placesItems.get(i).getGps_lat(),
                                placesItems.get(i).getGps_long(),
                                placesItems.get(i).getName(),
                                placesItems.get(i).getMunicipalities());


                        Log.d("longitude",placesItems.get(i).getGps_long().toString());

                        latlngs.add(new LatLng(placesItems.get(i).getGps_lat(), placesItems.get(i).getGps_long()));
                    }

                    // to make sure the markers are near to ech other
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();

                    builder.include(latlngs.get(1));
                    builder.include(latlngs.get(2));
                    builder.include(latlngs.get(0));
                    builder.include(latlngs.get(3));
                    builder.include(latlngs.get(4));
                    builder.include(latlngs.get(5));
                    builder.include(latlngs.get(6));


                    LatLngBounds bounds = builder.build();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));


                }

                @Override
                public void onFailure(Call<List<PlacesItem>> call, Throwable t) {
                    Snackbar mySnackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),"No Internet Connection",Snackbar.LENGTH_INDEFINITE);
                    mySnackbar.setAction("TRY AGAIN",new tryAgainListener());
                    mySnackbar.show();
                }
            });
        }

        protected Marker createMarker(double lat, double lon, String title, String snippet){
            googleMap.setOnInfoWindowClickListener(this);

            LatLng latLng=new LatLng(lat, lon);
           myMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(title)
                    .snippet(snippet)
            );
            return  myMarker;


        }


        @Override
        public void onResume() {
            super.onResume();
            mMapView.onResume();
        }

        @Override
        public void onPause() {
            super.onPause();
            mMapView.onResume();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mMapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mMapView.onLowMemory();
        }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.i("GoogleMapActivity", "onMarkerClick");
        Intent intent=new Intent(getActivity(), DetailActivity.class);
//        Toast.makeText(getActivity(),
//                "Marker Clicked: " + marker.getId(), Toast.LENGTH_LONG)
//                .show();

        switch (marker.getTitle()){
            case "Tripureshwor":   intent.putExtra("id",placesItems.get(0).getId());
                            intent.putExtra("name",placesItems.get(0).getName());
                            intent.putExtra("municipalities",placesItems.get(0).getMunicipalities());
                            intent.putExtra("ward",placesItems.get(0).getWard());
                            intent.putExtra("published_date",placesItems.get(0).getPublished_date());
                            startActivity(intent);
                            break;

            case "Chhetrapati":   intent.putExtra("id",placesItems.get(1).getId());
                intent.putExtra("name",placesItems.get(1).getName());
                intent.putExtra("municipalities",placesItems.get(1).getMunicipalities());
                intent.putExtra("ward",placesItems.get(1).getWard());
                intent.putExtra("published_date",placesItems.get(1).getPublished_date());
                startActivity(intent);
                break;

            case "Maharajgunj":   intent.putExtra("id",placesItems.get(2).getId());
                intent.putExtra("name",placesItems.get(2).getName());
                intent.putExtra("municipalities",placesItems.get(2).getMunicipalities());
                intent.putExtra("ward",placesItems.get(2).getWard());
                intent.putExtra("published_date",placesItems.get(2).getPublished_date());
                startActivity(intent);
                break;
            case "Mahankaal Chaur":   intent.putExtra("id",placesItems.get(3).getId());
                intent.putExtra("name",placesItems.get(3).getName());
                intent.putExtra("municipalities",placesItems.get(3).getMunicipalities());
                intent.putExtra("ward",placesItems.get(3).getWard());
                intent.putExtra("published_date",placesItems.get(3).getPublished_date());
                startActivity(intent);
                break;
            case "Kamaladi":   intent.putExtra("id",placesItems.get(4).getId());
                intent.putExtra("name",placesItems.get(4).getName());
                intent.putExtra("municipalities",placesItems.get(4).getMunicipalities());
                intent.putExtra("ward",placesItems.get(4).getWard());
                intent.putExtra("published_date",placesItems.get(4).getPublished_date());
                startActivity(intent);
                break;
            case "Baneshwor":   intent.putExtra("id",placesItems.get(5).getId());
                intent.putExtra("name",placesItems.get(5).getName());
                intent.putExtra("municipalities",placesItems.get(5).getMunicipalities());
                intent.putExtra("ward",placesItems.get(5).getWard());
                intent.putExtra("published_date",placesItems.get(5).getPublished_date());
                startActivity(intent);
                break;
            case "Bhaktapur":   intent.putExtra("id",placesItems.get(6).getId());
                intent.putExtra("name",placesItems.get(6).getName());
                intent.putExtra("municipalities",placesItems.get(6).getMunicipalities());
                intent.putExtra("ward",placesItems.get(6).getWard());
                intent.putExtra("published_date",placesItems.get(6).getPublished_date());
                startActivity(intent);
                break;
            case "Lalitpur":   intent.putExtra("id",placesItems.get(7).getId());
                intent.putExtra("name",placesItems.get(7).getName());
                intent.putExtra("municipalities",placesItems.get(7).getMunicipalities());
                intent.putExtra("ward",placesItems.get(7).getWard());
                intent.putExtra("published_date",placesItems.get(7).getPublished_date());
                startActivity(intent);
                break;
            case "Madhyapur Thimi":   intent.putExtra("id",placesItems.get(8).getId());
                intent.putExtra("name",placesItems.get(8).getName());
                intent.putExtra("municipalities",placesItems.get(8).getMunicipalities());
                intent.putExtra("ward",placesItems.get(8).getWard());
                intent.putExtra("published_date",placesItems.get(8).getPublished_date());
                startActivity(intent);
                break;
            case "Kirtipur":   intent.putExtra("id",placesItems.get(9).getId());
                intent.putExtra("name",placesItems.get(9).getName());
                intent.putExtra("municipalities",placesItems.get(9).getMunicipalities());
                intent.putExtra("ward",placesItems.get(9).getWard());
                intent.putExtra("published_date",placesItems.get(9).getPublished_date());
                startActivity(intent);
                break;

            case "Bansbari" :
                intent.putExtra("id",placesItems.get(10).getId());
                intent.putExtra("name",placesItems.get(10).getName());
                intent.putExtra("municipalities",placesItems.get(10).getMunicipalities());
                intent.putExtra("ward",placesItems.get(10).getWard());
                intent.putExtra("published_date",placesItems.get(10).getPublished_date());
                startActivity(intent);
                break;

        }
    }
public  class tryAgainListener implements View.OnClickListener{

    @Override
    public void onClick(View view) {
        getActivity().finish();
        startActivity(getActivity().getIntent());
    }
}

}


//