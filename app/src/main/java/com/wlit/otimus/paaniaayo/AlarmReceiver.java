package com.wlit.otimus.paaniaayo;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.wlit.otimus.paaniaayo.Activities.AlarmActivity;



/**
 * Created by Otimus on 2/16/2017.
 */

public class AlarmReceiver extends WakefulBroadcastReceiver {
    private static Ringtone ringtone;
    @Override
    public void onReceive(Context context, Intent intent) {
        //update UI with message
        AlarmActivity inst=AlarmActivity.instance();
        inst.setAlarmText("Wake Up");
        //use alarm tone
        Uri alarmUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if(alarmUri==null){
            alarmUri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        }
       ringtone =RingtoneManager.getRingtone(context,alarmUri);
        ringtone.play();

        //send notification message
        ComponentName comp = new ComponentName(context.getPackageName(),
                AlarmService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }

    public static void stopRingtone() {
        try {
            ringtone.stop();
        }
        catch (Exception e){

        }
    }

}

